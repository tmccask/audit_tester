package com.sample.tester.Audit_Tester.commands;

import org.apache.karaf.shell.api.action.Action;
import org.apache.karaf.shell.api.action.Command;
import org.apache.karaf.shell.api.action.lifecycle.Service;

@Command(scope="audit", name="test", description="Prints test message.")
@Service
public class TestCommand implements Action {

	public TestCommand() {
	}

	public Object execute() throws Exception {
		
		System.out.println("Test command ran.");
		
		return null;
	}

}
