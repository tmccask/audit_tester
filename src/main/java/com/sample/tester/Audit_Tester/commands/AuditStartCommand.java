package com.sample.tester.Audit_Tester.commands;

import org.apache.karaf.shell.api.action.Action;
import org.apache.karaf.shell.api.action.Command;
import org.apache.karaf.shell.api.action.lifecycle.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sample.tester.Audit_Tester.DBServiceFactory;
import com.sample.tester.Audit_Tester.api.AuditConnector;


@Command(scope="audit", name="start", description="Prints test message.")
@Service
public class AuditStartCommand implements Action {
	
	private static Logger log = LoggerFactory.getLogger(AuditStartCommand.class);

	public AuditStartCommand() {
	}

	public Object execute() throws Exception {
		log.info("Fetching DBServiceFactory.");
		System.out.println("Fetching DBServiceFactory.");
		System.out.flush();
		DBServiceFactory factory = DBServiceFactory.getInstance();
		log.info("Fetching Audit Connector.");
		System.out.println("Fetching Audit Connector.");
		System.out.flush();
		AuditConnector audit = factory.getDBConnector(AuditConnector.class);
		log.info("Writing audit.");
		System.out.println("Writing audit.");
		System.out.flush();
		audit.addAudit("Audit Tester", "Start", null, null, null, null, "Test line.");
		log.info("Audit Done.");
		System.out.println("Audit Done.");
		System.out.flush();
		return null;
	}

}
