package com.sample.tester.Audit_Tester.connectors;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import com.sample.tester.Audit_Tester.api.AuditConnector;
import com.sample.tester.Audit_Tester.beans.AuditBean;

import jline.internal.Log;

@Transactional
public class AuditConnectorImpl implements AuditConnector{
	
	@PersistenceContext(unitName="audit-test-unit")
	EntityManager entityManager;

	/**Adds an audit to the database. required fields are userId and type, all others are optional and can be left null; 
	 * @param userId
	 * @param type
	 * @param sourceProjectId
	 * @param sourceId
	 * @param messageId
	 * @param correlationId
	 * @param data
	 */
	public void addAudit(String userId, String type, Long sourceProjectId, String sourceId, String messageId, String correlationId, String data) {
		if (userId == null || type == null) {
			throw new NullPointerException("An audit is required to have the user id and the audit type.");
		}
		if (entityManager == null) {
			throw new NullPointerException("Entity Manager has not been set. Cannot save audit.");
		}
		AuditBean audit = new AuditBean(userId, type, sourceProjectId, sourceId, messageId, correlationId, data);
		entityManager.persist(audit);
	}
	
	public List<AuditBean> getAllAudits() {
		TypedQuery<AuditBean> aquery = entityManager.createQuery("SELECT x FROM AuditDAO x", AuditBean.class);
		List<AuditBean> result = aquery.getResultList();
		return result;
	}
	
	public AuditBean getAuditById(long id) {
		AuditBean result = entityManager.find(AuditBean.class, id);
		return result;
	}
	
}
