package com.sample.tester.Audit_Tester.beans;


import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "SMX_JCI_AUDIT")
public class AuditBean {

	public AuditBean() {
	}

	public AuditBean(String userId, String type) {
		this.userId = userId;
		this.type = type;
	}

	public AuditBean(String userId, String type, Long source, String sourceId, String messageId, String correlationId, String data) {
		this.userId = userId;
		this.type = type;
		this.source = source;
		this.sourceId = sourceId;
		this.messageId = messageId;
		this.correlationId = correlationId;
		this.data = data;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="Audit_Seq_Gen")
	@SequenceGenerator(name="Audit_Seq_Gen", sequenceName="SMX_JCI_AUDIT_ID_SEQ")
	@Column(name = "ID")
	private long id;

	@Column(name = "USER_ID")
	private String userId;

	@Column(name = "OCCURRENCE")
	private Timestamp occurrence;

	@Column(name = "TYPE")
	private String type;

	@Column(name = "SOURCE_PROJECT")
	private Long source;

	@Column(name = "SOURCE_ID")
	private String sourceId;

	@Column(name = "MESSAGE_ID")
	private String messageId;

	@Column(name = "CORRELATION_ID")
	private String correlationId;

	@Column(name = "DATA", columnDefinition = "CLOB")
	private String data;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Timestamp getOccurrence() {
		return occurrence;
	}

	public void setOccurrence(Timestamp occurrence) {
		this.occurrence = occurrence;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getSource() {
		return source;
	}

	public void setSource(Long source) {
		this.source = source;
	}

	public String getSourceId() {
		return sourceId;
	}

	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getCorrelationId() {
		return correlationId;
	}

	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "AuditDAO [id=" + id + ", userId=" + userId + ", occurrence="
				+ occurrence + ", type=" + type + ", source=" + source
				+ ", sourceId=" + sourceId + ", messageId=" + messageId
				+ ", correlationId=" + correlationId + ", data=" + data + "]";
	}

}
