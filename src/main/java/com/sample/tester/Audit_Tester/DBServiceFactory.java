package com.sample.tester.Audit_Tester;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DBServiceFactory {
    
    private static final Logger LOG = LoggerFactory.getLogger(DBServiceFactory.class);
    
    private static DBServiceFactory instance;
    
    private BundleContext bundleContext;
    
    public static DBServiceFactory getInstance() {
        if(instance == null) {
            instance = new DBServiceFactory();
        }
        return instance;
    }

    private DBServiceFactory() {
        super();
    }
    
    public void setBundleContext(BundleContext bundleContext) {
        LOG.info("Setting database service factory bundle context.");
        this.bundleContext = bundleContext;
    }
    
    public <T> T getDBConnector(Class<T> clazz) {
        
        if(bundleContext == null) {
            LOG.error("Bundle context is null, cannot instantiate connector.");
            throw new NullPointerException("Bundle context is null in Database service factory.");
        }
        
        ServiceReference<T> serviceRef = bundleContext.getServiceReference(clazz);
        
        if(serviceRef == null) {
            LOG.error("Could not instantiate service for "+clazz.getName());
            throw new NullPointerException(clazz.getName()+" service reference is null");
        }
        
        return bundleContext.getService(serviceRef);
    }

}
