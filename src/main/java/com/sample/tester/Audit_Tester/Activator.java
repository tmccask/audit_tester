package com.sample.tester.Audit_Tester;

import org.apache.karaf.util.tracker.BaseActivator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sample.tester.Audit_Tester.api.AuditConnector;
import com.sample.tester.Audit_Tester.connectors.AuditConnectorImpl;

public class Activator extends BaseActivator {
	
	private static final Logger log = LoggerFactory.getLogger(Activator.class);

	protected void doStart() throws Exception {
        log.info("Starting Audit Tester Bundle");
        DBServiceFactory.getInstance().setBundleContext(bundleContext);
        
	}

	protected void doStop() {
        log.info("Stopping Audit Tester Bundle");
		super.doStop();
	}

}
