package com.sample.tester.Audit_Tester.api;

import java.util.List;

import com.sample.tester.Audit_Tester.beans.AuditBean;


public interface AuditConnector {
	public void addAudit(String userId, String type, Long sourceProjectId, String sourceId, String messageId, String correlationId, String data);
	public List<AuditBean> getAllAudits();
	public AuditBean getAuditById(long id);
}
